Breast Cancer Wisconsin (Diagnostic) Dataset Analysis

Project Overview
This project involves the development of machine learning models to classify tumors into malignant (M) or benign (B) based on the features extracted from digitized images of breast mass. We utilize the Wisconsin Diagnostic Breast Cancer (WDBC) dataset for training and testing our models. Our goal is to achieve high accuracy in predicting the diagnosis, aiding in the early detection and treatment planning for breast cancer patients.

Dataset
The WDBC dataset consists of 569 samples with 32 attributes each. Attributes include an ID number, diagnosis, and 30 numerical features calculated from a digitized image of a fine needle aspirate (FNA) of a breast mass. Features describe characteristics of the cell nuclei present in the image.

Models Developed
Random Forest Classifier: This model uses an ensemble of decision trees to improve predictive accuracy and control over-fitting. It demonstrates robust performance in handling the dataset's complexity.

Support Vector Classifier (SVC): Employing a linear kernel, this model excels in finding the hyperplane that best separates the data into two classes, providing another effective approach to tumor classification.
